from collections import namedtuple

WORKERS = 4
JOBS = 4

time_cost = [[9, 2, 7, 8],
             [6, 4, 3, 7],
             [5, 8, 1, 8],
             [7, 6, 9, 4]]  # time_cost[worker][job]

Assignment = namedtuple('Assignment', 'worker job')


class Node:

    def __init__(self, cost=0, worker=-1, job=-1, parent=None):
        self.cost = cost
        self.worker = worker
        self.job = job
        self.parent = parent

    @staticmethod
    def delete_assigned_works(children, node):
        res = children
        as_list = node.get_assigned_jobs()
        for e in as_list:
            for n in children:
                if n.job == e.job:
                    res.remove(n)
        return res

    def get_assigned_jobs(self):
        res = []
        node = self
        while node is not None:
            if node.job >= 0 and node.worker >= 0:
                res.append(Assignment(node.worker, node.job));
            node = node.parent
        return res

    def get_children(self):
        children = []
        if self.worker == len(time_cost) - 1:
            return children
        worker = self.worker + 1
        children.append(Node(self.cost + time_cost[worker][0], worker=worker, job=0, parent=self))
        children.append(Node(self.cost + time_cost[worker][1], worker=worker, job=1, parent=self))
        children.append(Node(self.cost + time_cost[worker][2], worker=worker, job=2, parent=self))
        children.append(Node(self.cost + time_cost[worker][3], worker=worker, job=3, parent=self))
        return Node.delete_assigned_works(children, self)

    def __str__(self):
        return "Job : " + str(self.job) + " Worker: " + str(self.worker) + " Cost: " + str(self.cost)
