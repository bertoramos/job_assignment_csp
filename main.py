
import solver
import utils

if __name__=='__main__':
    node, visited_nodes = solver.solve()
    if node is None:
        print("No solution")
        exit(0)
    print("Total time : " + str(node.cost))
    assignments = node.get_assigned_jobs()
    print(assignments)
    print("Visited nodes -> " + str(visited_nodes))
