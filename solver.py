from utils import *


def solve():
    open_list = [Node()]

    min_node_cost = None
    visited_nodes = 0

    while len(open_list) != 0:
        node = open_list.pop(0)
        visited_nodes += 1
        if node.worker == WORKERS - 1:
            if min_node_cost is None or min_node_cost.cost > node.cost:
                min_node_cost = node
        else:
            open_list.extend(sorted(node.get_children(), key=lambda x: x.cost))
    return min_node_cost, visited_nodes
